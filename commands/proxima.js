const { store } = require("../store");

module.exports = [
  {
    name: "proxima",
    description: "Proxima musica.",
    async execute(message) {
      if (store.getDispatcher()) {
        store.getDispatcher().destroy();
        store.next();
      } else {
        message.channel.send("Não há uma musica sendo reproduzida caralho!");
      }
    },
  },
];
