const { store } = require("../store");

module.exports = [
  {
    name: "volume",
    description: "Muda o volume da creuza.",
    async execute(message, args) {
      if (store.getDispatcher()) {
        store.setVolume(parseInt(args[0]) / 100);
        store.getDispatcher().setVolume(store.getVolume());
      } else {
        message.channel.send("Não há uma musica sendo reproduzida caralho!");
      }
    },
  },
];
