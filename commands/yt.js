const { store } = require("../store");
const { getTitle } = require("../api/yt");

module.exports = [
  {
    name: "yt",
    description: "Toca musica do youtube.",
    async execute(message, args) {
      const id = args[0];
      store.setChannel(message.channel);

      if (!id) {
        message.reply("por favor, coloque um id válido");
        return;
      }

      if (message.member.voice.channel) {
        store.setVoiceChannel(message.member.voice.channel);
        getTitle(id).then((title) =>
          store.addPlaylist({
            id: id,
            user: message.member.displayName,
            title: title,
          })
        );
      } else {
        console.log("Nao Conectou");
        return;
      }
    },
  },
];
