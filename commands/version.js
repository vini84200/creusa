function showVersion(message) {
  message.reply(
    `a versão da Creusa que está rodando é a ${process.env.npm_package_version}`
  );
}

module.exports = [
  {
    name: "version",
    async execute(message) {
      showVersion(message);
    },
  },
  {
    name: "v",
    async execute(message) {
      showVersion(message);
    },
  },
  {
    name: "versao",
    async execute(message) {
      showVersion(message);
    },
  },
];
