const { store } = require("../store");
const Events = require("../consts/events")

store.on(Events.CHANGE_MUSIC, () => {
  if (store.getReplay()) {
    store.addPlaylist(store.getPlaying());
  }
});

module.exports = [
  {
    name: "replay",
    description: "Muda o modo de replay",
    async execute(message, args) {
      if (store.getReplay()) {
        store.setReplay(false);
        message.reply(
          "Você saiu do modo de repetição de musica. Para voltar use !replay "
        );
      } else {
        store.setReplay(true);
        if (store.getPlaying()) {
          store.addPlaylist(store.getPlaying());
        }
        message.reply(
          "Você entrou no modo de repetição de musica. Para sair use !replay"
        );
      }
    },
  },
];
