const { store } = require("../store");

module.exports = [
  {
    name: "pause",
    description: "Pausa a musica.",
    async execute(message) {
      if (store.getDispatcher()) {
        store.pause();
      } else {
        message.channel.send("Não há uma musica sendo reproduzida caralho!");
      }
    },
  },
  {
    name: "retomar",
    description: "Retoma a musica.",
    async execute(message) {
      if (store.getDispatcher()) {
        store.play();
      } else {
        message.channel.send("Não há uma musica sendo reproduzida caralho!");
      }
    },
  },
];
