const { store } = require("../store");

module.exports = [
  {
    name: "url",
    async execute(message) {
      message.channel.send(`A URL é: **${store.getUrl()}**`);
    },
  },
];
