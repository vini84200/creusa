const { store } = require("../store");

module.exports = [
  {
    name: "np",
    description: "Mostra o que esta tocando.",
    async execute(message) {
      if (store.getPlaying()) {
        message.channel.send("Ta tocando **" + store.getPlaying().title + "**");
      } else {
        message.channel.send("Não há uma musica sendo reproduzida caralho!");
      }
    },
  },
];
