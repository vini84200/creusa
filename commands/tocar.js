const { store } = require("../store");
const { getOptions } = require("../api/yt");

function avaliarResposta(message, collector, options) {
  if (message.content == "sair") {
    message.reply("Desistindo...");
    collector.stop();
    return;
  }

  const opcaoNum = parseInt(message.content);

  if (opcaoNum < 1 || opcaoNum > options.length || isNaN(opcaoNum)) {
    message.reply(`Responda com um número de 1 ate ${options.length}!`);
    return;
  }

  let videoEscolhido = options[opcaoNum - 1];
  if (message.member.voice.channel) {
    store.setVoiceChannel(message.member.voice.channel);
    store.addPlaylist({
      id: videoEscolhido.id.videoId,
      user: message.member.displayName,
      title: videoEscolhido.snippet.title,
    });
  } else {
    message.reply("Não foi possível conectar à seu canal de voz.");
    return;
  }
  collector.stop();
}

const searchVidio = async (query, message) => {
  const options = await getOptions(query);

  const optionsString = options.reduce(
    (accumulator, option, index) =>
      accumulator + `${index + 1}- ${option.snippet.title} \n`,
    ""
  );

  store
    .getChannel()
    .send(
      `Aqui está os resultados, porra: \n ${optionsString} *Envie 'sair' para desisitir*`
    );

  // Coleta msgs do usuario para esperar resposta
  const collector = message.channel.createMessageCollector(
    (m) => m.author.id === message.author.id
  );

  collector.on("collect", (message) => {
    avaliarResposta(message, collector, options);
  });
};

module.exports = [
  {
    name: "tocar",
    description: "Pesquisa a musica no yt.",
    async execute(message, args) {
      const query = args.join(" ");

      if (!query) {
        message.reply("Coloque um nome de uma música para pesquisar.");
        return;
      }

      store.setChannel(message.channel);
      await searchVidio(query, message);
    },
  },
];
