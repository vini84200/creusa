const { store } = require("../store");
const { reduce } = require("./tocar");

module.exports = [
  {
    name: "queue",
    description: "Retoma a musica.",
    async execute(message) {
      var musicPlaying = store.getPlaying();
      var musicTitles = store.getPlaylist().map((music) => music.title);

      if (musicPlaying == undefined) {
        message.channel.send("Não há uma musica sendo reproduzida caralho!");
      } else {
        if (
          (musicTitles == "") |
          (musicTitles == []) |
          (musicTitles == undefined)
        ) {
          message.channel.send("Não vou cantar mais nada depois dessa!");
        } else {
          var text = musicTitles.reduce((acc, crv) => {
            return acc + `-${crv};\n`;
          }, "");
          message.channel.send(
            `Estou cantando **${musicPlaying.title}** e se não gostou vai se fuder! E depois vou cantar:\n ${text}`
          );
        }
      }
    },
  },
];
