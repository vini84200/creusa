const express = require("express");
const localtunnel = require("localtunnel");
const cors = require('cors')
var morgan = require("morgan");
const socketIO = require("socket.io");
const HTTP = require("http");
const Events = require("./consts/events")

const { store } = require("./store");

require("dotenv").config({ path: ".env" });

const port = process.env.PORT || 8000;

module.exports = function WebApp() {
  const app = express();
  const http = HTTP.createServer(app);
  const io = socketIO(http, { origins: '*:*',});
  let url;
  app.use(cors({ origin: '*' }))
  app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.header('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', false);

    // Pass to next layer of middleware
    next();
});

  app.use(express.static("creusa_web/dist"));
  app.use(morgan("dev"));

  // VIEWS

  // SOCKET IO

  io.on("connection", (socket) => {
    socket.emit("musicChange", store.getPlaying());
    socket.emit("queueUpdate", store.getPlaylist());
    socket.emit("changeStatus", store.getStatus());

    socket.on("play", () => {
      store.play();
    });
    socket.on("pause", () => {
      store.pause();
    });
    socket.on("next", () => {
      store.next();
    });
  });

  store.on(Events.CHANGE_MUSIC, () => {
    io.emit("musicChange", store.getPlaying());
  });

  store.on(Events.QUEUE_UPDATE, () => {
    io.emit("queueUpdate", store.getPlaylist());
  });

  store.on(Events.CHANGE_PLAYING_STATUS, () => {
    io.emit("changeStatus", store.getStatus());
  });

  // CONFIG

  http.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
  });

  

  if (process.env.NODE_ENV == "DEV") {
    store.setUrl(`http://localhost:${port}`);
  } else {
    store.setUrl(`http://18.228.245.101:8000/`);
  }

  // if (process.env.NODE_ENV == "DEV") {
  //   url = `http://localhost:${port}`;
  //   store.setUrl(url);
  // } else {
  //   localtunnel({ port: port }).then((tunnel) => {
  //     console.log(tunnel.url);
  //     url = tunnel.url;
  //     store.setUrl(url);
  //   });
  // }

  return {};
};
