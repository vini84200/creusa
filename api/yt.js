const axios = require("axios");
const ytkey = "AIzaSyBwGUVhc8KVjr1xXZ3W2Im7VXZYHuHYuSo";

async function getOptions(query, count = 9) {
  let request = await axios.get(
    `https://www.googleapis.com/youtube/v3/search?part=snippet&q=${encodeURI(
      query
    )}&type=video&maxResults=${count}&key=${ytkey}`
  );
  return request.data.items;
}

const getTitle = async (id) => {
  return (
    await axios.get(
      `https://www.googleapis.com/youtube/v3/videos?part=snippet&id=${id}&fields=items(id%2Csnippet)&key=${ytkey}`
    )
  ).data.items[0].snippet.title;
};

module.exports = {
  getTitle,
  ytkey,
  getOptions,
};
