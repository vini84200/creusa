export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Creusa Web',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/fontawesome'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: ['nuxt-socket-io'],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
  io: {
    sockets: [
      // Required
      {
        // At least one entry is required
        name: 'creusa',
        url: process.env.VUE_APP_SOCKET_LINK,
        default: true,
        vuex: {
          mutations: [
            'connect --> SOCKET_connect',
            'disconnect --> SOCKET_disconnect',
            'musicChange --> SOCKET_musicChange',
            'queueUpdate --> SOCKET_queueUpdate',
            'changeStatus --> SOCKET_changeStatus'
          ]
        }
      }
    ]
  },
  fontawesome: {
    icons: {
      solid: [
        'faExternalLinkAlt',
        'faPlay',
        'faPause',
        'faStepForward',
        'faStepBackward'
      ]
    }
  }
}
