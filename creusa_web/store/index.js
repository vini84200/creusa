export const state = () => ({
  isConnected: false,
  music: null,
  playlist: [],
  playingStatus: 'playing'
})

export const mutations = {
  SOCKET_connect(state) {
    console.log('Connected')
    state.isConnected = true
  },
  SOCKET_disconnect(state) {
    console.log('Disconnected')
    state.isConnected = false
  },
  SOCKET_musicChange(state, music) {
    state.music = music
  },
  SOCKET_queueUpdate(state, queue) {
    state.playlist = queue
  },
  SOCKET_changeStatus(store, status) {
    store.playingStatus = status
  }
}
