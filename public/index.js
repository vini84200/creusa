$(function () {
  var socket = io();

  socket.on("musicChange", (music) => {
    if (!music) {
      $("#nowPlaying").text("Nada tocando");
      return;
    }
    $("#nowPlaying").html(
      `Tocando <b>${music.title}</b>, sugerido por ${music.user}`
    );
  });
});
