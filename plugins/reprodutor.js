const { store } = require("../store");
const ytdl = require("ytdl-core-discord");

const Events = require("../consts/events")

console.log("Loading plugin reprodutor");

async function play(video) {
  const url = `https://www.youtube.com/watch?v=${video.id}`;
  try {
    store.setDispatcher(
      store.getConnection().play(await ytdl(url), {
        type: "opus",
        volume: store.getVolume(),
        passes: 2,
      })
    );
  } catch (e) {
    store.getChannel().send("Não foi possivel tocar a musica.");
    store.next();
    console.error(e);
    console.log(url, store.getDispatcher(), store.getConnection());
  }

  function onChangeStatus() {
    if (store.getStatus() == "paused") {
      store.getDispatcher().pause(true);
    } else {
      store.getDispatcher().resume(true);
    }
  }
  store.getDispatcher().on("finish", () => {
    store.next();
    store.off(Events.CHANGE_PLAYING_STATUS, onChangeStatus);
  });
  store.getDispatcher().on("error", console.error);
  store.getDispatcher().on("start", () => {
    store.on(Events.CHANGE_PLAYING_STATUS, onChangeStatus);
    store
      .getChannel()
      .send(
        "Começando a tocar **" +
          video.title +
          "**, recomendado por **" +
          video.user +
          "**"
      );
    console.log(video.title);
  });
}

store.on(Events.CHANGE_MUSIC, function ChangeMusic() {
  if (store.getDispatcher()) {
    // Destroi o reprodutor, se ele já existir
    store.getDispatcher().destroy();
  }
  if (!store.getPlaying()) {
    return;
  }
  play(store.getPlaying());
});

store.on(Events.PLAYLIST_END, function playlistEnd() {
  store.setPlaying(undefined);
  if (store.getChannel()) {
    store.getChannel().send("Fim da fila, desconectando");
  } else {
    console.error("Sem channel");
  }
  store.getConnection().disconnect();
  store.setConnection(undefined);
});

store.on(Events.ADD_TO_QUEUE, function addQueue() {
  // Quando um novo item é adicionado a uma lista vazia, ele deve iniciar a reprodução

  if (store.getConnection() && store.getPlaying) {
    return;
  }
  if (store.getConnection()) {
    return store.next();
  }
  if (!store.getVoiceChannel()) {
    return console.error("Não há canal");
  }
  store
    .getVoiceChannel()
    .join()
    .then((c) => {
      store.setConnection(c);
      store.next();
    });
});

store.on(Events.ADD_TO_QUEUE, function (music) {
  store
    .getChannel()
    .send(
      `Musica **${music.title}** adicionada a playlist por recomendação de ${music.user}`
    );
});
