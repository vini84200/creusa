// require the discord.js module
const Discord = require("discord.js");
const fs = require("fs");
const webApp = require("./webApp");
const { prefix, token } = require("./config.json");
const randomActivity = require("./randomActivity");
require("dotenv").config({ path: ".env" });
// create a new Discord client

// login to Discord with your app's token

const Sentry = require("@sentry/node");

console.log(`Iniciando creusa v. ${process.env.npm_package_version}`);

Sentry.init({
  dsn:
    "https://62f4b44f135c4775a5475aa9ef988283@o244611.ingest.sentry.io/5450498",

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
  environment: process.env.NODE_ENV,
  release: `creusa-backend@${process.env.npm_package_version}`,
});

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs
  .readdirSync("./commands")
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const commands = require(`./commands/${file}`);
  for (const command of commands) {
    client.commands.set(command.name, command);
  }
}

const pluginFiles = fs
  .readdirSync("./plugins")
  .filter((file) => file.endsWith(".js"));

for (const file of pluginFiles) {
  require(`./plugins/${file}`);
}

client.once("ready", () => {
  randomActivity(client);
  console.log("Ready!");
});

client.on("message", (message) => {
  if (!message.content.startsWith(prefix) || message.author.bot) return;

  const args = message.content.slice(prefix.length).split(/ +/);
  const command = args.shift().toLowerCase();

  if (!client.commands.has(command)) return;

  try {
    client.commands.get(command).execute(message, args);
  } catch (error) {
    console.error(error);
    message.reply("there was an error trying to execute that command!");
  }
});

client.on("message", async (message) => {
  if (
    message.content === "limpa ai" ||
    message.content === "Limpa ai" ||
    message.content === "limpa aí" ||
    message.content === "Limpa aí"
  ) {
    message.channel.bulkDelete(100);
    message.channel.send("To limpando já cacete! Todo dia a mesma merda!");
  }
  if (
    message.content === "bom dia" ||
    message.content === "Bom dia" ||
    message.content === "bom Dia" ||
    message.content === "Bom Dia"
  ) {
    message.channel.send("Bom dia é o caralho! Queria mesmo é estar dormindo");
  }
});

client.login(token);
webApp();
