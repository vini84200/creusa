# Creusa

A Creusa foi criada como um bot para o Discord com multiplas funções, como reproduzir música e automatizar tarefas repetitivas.

## [Unreleased]


## [0.1.3] - 2021-01-02

### Corrigido
    - Erro 1G - causando o Status Code 416, o que levava a um Crash da Creusa
    - Problema com a conexão com o Youtube(atualizado o ytdl)

## [0.1.2] - 2020-11-20

### Adicionado

    - Modo de Replay(!replay)

### Modificado

    - Creusa agora possui uma URL permanente (https://creusa.netlify.app/)

### Corrigido

    - Comportamento do comando Queue(!queue)

## [0.1.1] - 2020-11-03

### Adicionado

    - Novas atividades para Creusa
    - Comando !versao, !v, !version para exibir a versão da Creusa.

### Modificado

    - Volume inicial passa a ser 3

### Corrigido

    - Erro de Crash quando executa música
    - Erro de sugestão aparecendo de null para alguns usuarios
    - Comando !tocar trata problemas melhor
    - Comando !yt trata id em branco
    - Comando !tocar trata pesquisa em branco

## [0.1.0] - 2020-11-01

### Added

    - Este Changelog
    - Comando '!tocar <nome>' que pesquisa uma música no Youtube e adiciona a lista de reprodução ao no canal de voz
    - Comando '!pausar' que pausa a reprodução
    - Comando '!retomar' que retoma a reprodução
    - Comando '!yt <id>' que adiciona a música a lista de reprodução
    - Lista de reproução, em que os videos são reproduzidos em ordem, como uma fila
    - Comando '!volume <vol>' que altera o volume( entre 0 e 100 )
    - Comando '!np' que mostra o que está sendo tocado
    - Uma página na internet, que é servida junto a Creusa
    - Forma de pausar e avançar a música do site
    - Visualização da lista de reprodução do site
    - Visualização do que está sendo reproduzido
    - Identificação de quem adicionou a música a lista de reprodução
    - Atividades aleatorias para o Bot do Discord
