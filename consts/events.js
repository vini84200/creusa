const events = {
    CHANGE_MUSIC: "changeMusic",
    PLAYLIST_END: "playlistEnd",
    QUEUE_UPDATE: "QueueUpadate",
    ADD_TO_QUEUE: "addQueue",
    CHANGE_PLAYING_STATUS: "changeStatus",
};
module.exports = events;