const Events = require('./consts/events')

function DispatcherEvent(newEventName) {
  eventName = newEventName;

  return {
    callbacks: [],
    registerCallback: function (callback) {
      this.callbacks.push(callback);
    },

    unregisterCallback: function (callback) {
      const index = this.callbacks.indexOf(callback);
      if (index > -1) {
        this.callbacks.splice(index, 1);
      }
    },

    fire: function (data) {
      const Callbacks = this.callbacks.slice(0);
      Callbacks.forEach((callback) => {
        callback(data);
      });
    },
  };
}

function store() {
  let events = {};
  let nowPlaying = undefined;
  let playlist = [];
  let history = [];
  let volume = 0.03;
  let voiceChannel;
  let link = "";
  let status = "playing";
  let dispatcher = undefined;
  let connection = undefined;
  let channel = undefined;
  let replay = false;
  return {
    // Sistema de Eventos
    dispatch(eventName, data) {
      const event = events[eventName];

      if (event) {
        event.fire(data);
      }
    },
    on(eventName, callback) {
      let event = events[eventName];

      if (!event) {
        event = DispatcherEvent(eventName);
        events[eventName] = event;
      }
      event.registerCallback(callback);
    },

    off(eventName, callback) {
      const event = events[eventName];
      if (event && event.callbacks.indexOf(callback) > -1) {
        event.unregisterCallback(callback);
        if (event.callbacks.length === 0) {
          delete events[eventName];
        }
      }
    },

    // Tocando agora
    getPlaying: () => {
      return nowPlaying;
    },
    setPlaying: function (music) {
      nowPlaying = music;
      this.dispatch(Events.CHANGE_MUSIC);
    },

    // Lista de reprodução
    next: function () {
      history.push(this.getPlaying);
      if (playlist.length == 0) {
        this.dispatch(Events.PLAYLIST_END);
      }
      this.setPlaying(playlist.shift());
      this.dispatch(Events.QUEUE_UPDATE);
    },
    getPlaylist: function () {
      return playlist;
    },
    addPlaylist: function (url) {
      playlist.push(url);
      this.dispatch(Events.ADD_TO_QUEUE, url);
      this.dispatch(Events.QUEUE_UPDATE);
    },

    // Volume
    getVolume: () => volume,
    setVolume: (newVolume) => {
      if (newVolume > 1) {
        return (volume = 1);
      }
      if (newVolume < 0) {
        return (volume = 0);
      }
      volume = newVolume;
    },

    // Canal de voz
    setVoiceChannel: function (vc) {
      voiceChannel = vc;
    },
    getVoiceChannel: function () {
      return voiceChannel;
    },

    // Url
    getUrl: function () {
      return link;
    },
    setUrl: function (url) {
      link = url;
    },
    // Status
    pause() {
      status = "paused";
      this.dispatch(Events.CHANGE_PLAYING_STATUS);
    },
    play() {
      status = "playing";
      this.dispatch(Events.CHANGE_PLAYING_STATUS);
    },
    getStatus() {
      return status;
    },
    getDispatcher() {
      return dispatcher;
    },
    setDispatcher(d) {
      dispatcher = d;
    },
    getConnection() {
      return connection;
    },
    setConnection(c) {
      connection = c;
    },
    getChannel() {
      return channel;
    },
    setChannel(c) {
      channel = c;
    },
    getReplay() {
      return replay;
    },
    setReplay(r) {
      replay = r;
    },
  };
}

module.exports.store = store();
